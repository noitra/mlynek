import java.io.*;
import java.util.*;
import java.net.*;

public class Main {
	Vector<String> users = new Vector<String>();
	Vector<HandleClient> clients = new Vector<HandleClient>();
	private boolean in_process = false;
	  
	public static void main(String[] args) throws Exception {
		new Main().process();
	}
	
	public void process() throws Exception {
		System.out.println("Startuje serwer...");
		ServerSocket server = new ServerSocket(6666);
		System.out.println("Server wystartował!");
		
		
        while( true) {
    		 Socket client = server.accept();
    		 HandleClient c = new HandleClient(client);
     		 clients.add(c);
        } 
	}
	
	public void boradcast(String user, String message)  {
		String user_tmp = user;
	    for ( HandleClient c : clients )
	       if ( !c.getUserName().equals(user)) {
//	    	   if (message.equals("command: yourturn")) user="";
//	   		   if (message.length() >=13 && message.substring(0, 13).equals("command: set:")) user="";
	    	   if (message.equals("process")) {
	    		   in_process = true;
	    		   user_tmp = "";
	    	   }
	    	   if (message.equals("end_process")) {
	    		   in_process = false;
	    		   user_tmp = "";
	    	   }
	    	   if (in_process == true) user_tmp = "";
	    	   c.sendMessage(user_tmp,message);
	       }
	}
	
	class  HandleClient extends Thread {
	    String name = "";
		BufferedReader input;
		PrintWriter output;

		public HandleClient(Socket  client) throws Exception {
			input = new BufferedReader( new InputStreamReader( client.getInputStream())) ;
			output = new PrintWriter ( client.getOutputStream(),true);
			name  = input.readLine();
			users.add(name); // add to vector
			System.out.println(name+": podlaczyl sie do serwera.");
			output.println("Witaj na serwerze "+name);
			if (clients.size() == 0) {
				output.println("Grasz białymi");
				output.println("process");
				output.println("white");
				output.println("end_process");
			}
			if (clients.size() == 1) {
				output.println("Grasz czarnymi");
				output.println("process");
				output.println("black");
				output.println("end_process");
			}
			if (clients.size() > 1) { 
				output.println("Niestety nie ma już miejsca w grze.");
				output.println("process");
				output.println("noone");
				output.println("end_process");
			}
			start();
        }

        public void sendMessage(String uname,String  msg)  {
        	if (uname.equals("")) output.println(uname + msg);
        	else output.println(uname + ":" + msg);
        }
		
        public String getUserName() {  
            return name; 
        }
        public void run()  {
        	String line;
        	try    {
                while(true)   {
					line = input.readLine();
					System.out.println(name+": "+line);
					if ( line.equals("end") ) {
						clients.remove(this);
					 	users.remove(name);
					 	break;
		            }
					boradcast(name,line); 
                } 
		     } 
		     catch(Exception ex) {
		    	 System.out.println(ex.getMessage());
		     }
        } 
   } 
}
