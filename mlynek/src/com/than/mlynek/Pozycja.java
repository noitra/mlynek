package com.than.mlynek;

import java.util.ArrayList;

public class Pozycja {
	static int count = 0;
	private int id = 0;
	private int x, y;
	private int szerokosc, dlugosc;
	private boolean zajeta;
	
	public Pozycja(int x, int y, int szerokosc, int dlugosc) {
		this.x = x;
		this.y = y;
		this.id = count;
		count++;
		this.szerokosc = szerokosc;
		this.dlugosc = dlugosc;
		this.zajeta = false;
	}
	
	public int getId() {
		return this.id;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public int getSzerokosc() {
		return szerokosc;
	}
	
	public int getDlugosc() {
		return dlugosc;
	}
	
	public boolean getZajeta() {
		return zajeta;
	}
	
	public void setZajeta(boolean zajeta) {
		this.zajeta = zajeta;
	}
	
	public boolean wykryjKolizje(int x, int y) {
		if(x > this.x && x < this.x + szerokosc && y > this.y && y < this.y + dlugosc)
			return true;
		return false;
	}
}